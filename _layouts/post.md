---
layout: default
---
{% assign post = page %}

<h1>{{ post.title }}</h1>

<p>{{ content }}</p>
